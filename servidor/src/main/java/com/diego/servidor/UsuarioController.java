package com.diego.servidor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Diego Lorente on 09/02/2017.
 */
@RestController
public class UsuarioController {

    @Autowired
    private UsuarioRepository repositorio;

    @RequestMapping("/user_login")
    public boolean getLogin(String usuario,String pass){
        Usuario usernombre=repositorio.findByUsuarioAndPass(usuario,pass);
        if (usernombre == null)
            return false;

        return true;
    }




    @RequestMapping("/add_user")
    public boolean addUser(@RequestParam(value="usuario",defaultValue = "nada")String usuario,
                           @RequestParam(value="pass",defaultValue="nada") String pass){

        if(repositorio.findByUsuario(usuario)!=null){return false;}
        Usuario user=new Usuario();
        user.setUsuario(usuario);
        user.setPass(pass);
        repositorio.save(user);
        return true;

    }
}
