package com.diego.servidor;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Diego Lorente on 09/02/2017.
 */
public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{

    List<Usuario> findAll();
    Usuario findByUsuarioAndPass(String usuario, String pass);
    Usuario findByUsuario(String usuario);
}
