package com.diego.servidor;

import javax.persistence.*;

/**
 * Created by Diego Lorente on 09/02/2017.
 */
@Entity
@Table
public class Usuario {

    @Id
    @GeneratedValue
    private int id;
    @Column
    private String usuario;
    @Column
    private String pass;


    public Usuario(){

    }





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
