package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.diego.myapplication.Activities.Util.Util;
import com.diego.myapplication.R;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

public class MapActivity extends AppCompatActivity {

    private MapView mapView;
    String nombre;
    double latitud, longitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        MapboxAccountManager.start(this, "pk.eyJ1IjoiZGllZ29sb3JlbnRlIiwiYSI6ImNpeWVwMWJzaDAwZTYzM3AxZ2xzbnIyMnMifQ.NjNlFSnIBeYbX6qt5K0QVQ");

        Intent intent = getIntent();
        nombre = intent.getStringExtra("nombre");
        latitud = intent.getDoubleExtra("latitud", 0);
        longitud = intent.getDoubleExtra("longitud", 0);

        mapView = (MapView) findViewById(R.id.mapaRestaurante);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                mapboxMap.addMarker(new MarkerOptions().position(new LatLng(Util.parseCoordenadas(String.valueOf(latitud), String.valueOf(longitud)))).title(nombre));

                CameraPosition position = new CameraPosition.Builder().target(new LatLng(Util.parseCoordenadas(String.valueOf(latitud), String.valueOf(longitud)))).zoom(17).tilt(30).build();
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 7000);
            }
        });

}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mapa_todos,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent=null;
        int id=item.getItemId();
        if(id== R.id.action_lista_restaurantes){
            intent=new Intent(this,ListaRestaurantes.class);
            startActivity(intent);
        }else if (id== R.id.action_lista_favoritos){
            intent=new Intent(this,ListaFavoritos.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}

