package com.diego.myapplication.Activities.Util;

/**
 * Created by Diego Lorente on 13/01/2017.
 */
public class Constantes {

    public static final String TABLA = "restaurantes";
    public static final String ID = "id";
    public static final String NOMBRE = "nombre";
    public static final String LATITUD = "latitud";
    public static final String LONGITUD = "longitud";
    public static final String VERSION="50.0 por lo menos";


    public static final String URL = "http://www.zaragoza.es/georref/json/hilo/ver_Restaurante";
    //public static final String SERVIDOR_URL = "http://192.168.1.103:8082";
    public static final String SERVIDOR_URL = "http://192.168.43.125:8082";

}
