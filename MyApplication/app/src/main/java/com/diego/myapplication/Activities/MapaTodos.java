package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.diego.myapplication.Activities.Util.Util;
import com.diego.myapplication.R;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

public class MapaTodos extends AppCompatActivity {
    private MapView mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapboxAccountManager.start(this, "pk.eyJ1IjoiZGllZ29sb3JlbnRlIiwiYSI6ImNpeWVwMWJzaDAwZTYzM3AxZ2xzbnIyMnMifQ.NjNlFSnIBeYbX6qt5K0QVQ");

        setContentView(R.layout.activity_mapa_todos);
        mapView = (MapView) findViewById(R.id.mapaTodos);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap mapboxMap) {
                for(Restaurante resta : ListaRestaurantes.listaRestaurantes){
                    mapboxMap.addMarker(new MarkerOptions().position(new LatLng(Util.parseCoordenadas(String.valueOf(resta.getLatitud()), String.valueOf(resta.getLongitud())))).title(resta.getNombre()));
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mapa_todos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.action_lista_restaurantes:
                intent = new Intent(this, ListaRestaurantes.class);
                startActivity(intent);
                break;
            case R.id.action_lista_favoritos:
                intent = new Intent(this, ListaFavoritos.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}









