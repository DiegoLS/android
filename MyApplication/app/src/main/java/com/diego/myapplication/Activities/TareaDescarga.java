package com.diego.myapplication.Activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.diego.myapplication.Activities.Util.Util;
import com.diego.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import uk.me.jstott.jcoord.LatLng;

import static com.diego.myapplication.Activities.ListaRestaurantes.adaptador;
import static com.diego.myapplication.Activities.ListaRestaurantes.listaRestaurantes;
import static com.diego.myapplication.Activities.Util.Constantes.URL;

/**
 * Created by borja on 19/12/2016.
 */

public class TareaDescarga extends AsyncTask<String,Void,Void> {

    private boolean error = false;
    private ProgressDialog dialog;
    private ListaRestaurantes activity;
    public TareaDescarga(ListaRestaurantes activity){
        this.activity=activity;
    }
    @Override
    protected Void doInBackground(String... strings) {
        InputStream is = null;
        String resultado = null;
        JSONObject json = null;
        JSONArray jsonArray = null;

        try {
            URL url = new URL(URL);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String linea = null;

            while ((linea = br.readLine()) != null)
                sb.append(linea + "\n");

            conexion.disconnect();
            br.close();
            resultado = sb.toString();

            json = new JSONObject(resultado);
            jsonArray = json.getJSONArray("features");

            String nombre = null;
            String coordenadas = null;
            Restaurante restaurante = null;

            for (int i = 0; i < jsonArray.length(); i++) {
                nombre = jsonArray.getJSONObject(i).getJSONObject("properties").getString("title");
                String link = jsonArray.getJSONObject(i).getJSONObject("properties").getString("link");
                String calle = jsonArray.getJSONObject(i).getJSONObject("properties").getString("description");
                String categoria = jsonArray.getJSONObject(i).getJSONObject("properties").getString("category");
                coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates");
                String[] arrayCoordenadas = coordenadas.substring(1, coordenadas.length() - 1).split(",");
                LatLng latLng = Util.DeUMTSaLatLng(
                        Double.parseDouble(arrayCoordenadas[0]),
                        Double.parseDouble(arrayCoordenadas[1]));

                restaurante = new Restaurante();
                restaurante.seturl(link);
                restaurante.setNombre(nombre);
                restaurante.setCalle(calle);
                restaurante.setCategoria(categoria);
                restaurante.setLatitud(latLng.getLat());
                restaurante.setLongitud(latLng.getLng());
                listaRestaurantes.add(restaurante);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            error = true;
        } catch (JSONException jse) {
            jse.printStackTrace();
            error = true;
        }

        return null;
    }
    @Override
    protected void onCancelled() {
        super.onCancelled();
        listaRestaurantes = new ArrayList<>();
    }
    @Override
    protected void onProgressUpdate(Void... progreso) {
        super.onProgressUpdate(progreso);
        adaptador.notifyDataSetChanged();
    }

    @Override
    protected void onPostExecute(Void resultado) {
        super.onPostExecute(resultado);

        if (error) {
            Toast.makeText(activity,R.string.error, Toast.LENGTH_SHORT).show();
            return;
        }

        adaptador.notifyDataSetChanged();
    }
}
