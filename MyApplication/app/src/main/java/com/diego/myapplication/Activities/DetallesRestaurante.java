package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.diego.myapplication.R;

public class DetallesRestaurante extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_restaurante);
        rellenarDetalles();
    }

    private void rellenarDetalles() {
        TextView tvNombre = (TextView) findViewById(R.id.tvNombreRestaurante);
        TextView tvCalle = (TextView) findViewById(R.id.tvCalleRestaurante);
        TextView tvDescripcion = (TextView) findViewById(R.id.tvDescripcionRestaurante);
        Intent intent = getIntent();
        tvNombre.setText(intent.getStringExtra("nombre"));
        tvCalle.setText(intent.getStringExtra("calle"));
        tvDescripcion.setText(intent.getStringExtra("categoria"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detalles, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.action_lista_restaurantes:
                intent = new Intent(this, ListaRestaurantes.class);
                startActivity(intent);
                break;
            case R.id.action_lista_favoritos:
                intent = new Intent(this, ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.action_mapa_todos:
                intent = new Intent(this, MapaTodos.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
