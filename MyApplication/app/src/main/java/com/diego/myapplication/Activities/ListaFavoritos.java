package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.diego.myapplication.R;

import java.util.ArrayList;

public class ListaFavoritos extends AppCompatActivity {

    private ListView lista;
    public static ArrayList<Restaurante> listaRestaurante;
    public static RestauranteAdapter adaptador;
    public static RestauranteAdapter adaptadorFavoritos;

    private BaseDatos db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_restaurantes_favoritos);
        lista = (ListView) findViewById(R.id.lvRestaurantesFavoritos);
        listaRestaurante=new ArrayList<>();
        db=new BaseDatos(this);
        lista.setAdapter(adaptador);
        actualizarRestaurantes();
        registerForContextMenu(lista);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mapa_todos,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent=null;
        int id=item.getItemId();
        if(id==R.id.action_lista_restaurantes){
            intent=new Intent(this,ListaRestaurantes.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getMenuInflater().inflate(R.menu.menu_lista_favoritos, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item){

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int itemSeleccionado = info.position;

        Intent intent = null;
        Restaurante restaurante;
        switch (item.getItemId()){
            case R.id.action_eliminar:
                restaurante = listaRestaurante.get(itemSeleccionado);
                db.eliminarRestaurante(restaurante);
                actualizarRestaurantes();
                break;
            case R.id.action_modificar:
                intent = new Intent(this, Modificar.class);
                restaurante = listaRestaurante.get(itemSeleccionado);
                intent.putExtra("nombre", restaurante.getNombre());
                intent.putExtra("calle", restaurante.getCalle());
                intent.putExtra("link", restaurante.geturl());
                intent.putExtra("categoria", restaurante.getCategoria());
                intent.putExtra("latitud", restaurante.getLatitud());
                intent.putExtra("longitud",restaurante.getLongitud());
                startActivity(intent);
                break;
            default:
                break;
        }
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        actualizarRestaurantes();
    }


    public void actualizarRestaurantes(){
        listaRestaurante=db.getRestaurantesFavoritos();
        adaptadorFavoritos=new RestauranteAdapter(this,listaRestaurante);
        lista.setAdapter(adaptadorFavoritos);

    }
}
