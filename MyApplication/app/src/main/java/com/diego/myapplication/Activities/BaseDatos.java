package com.diego.myapplication.Activities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import static com.diego.myapplication.Activities.Util.Constantes.TABLA;

/**
 * Created by Diego Lorente on 13/01/2017.
 */
public class BaseDatos extends SQLiteOpenHelper {

    private static final String NOMBRE_BASE = "restaurantes.db";
    private static final int VERSION = 2;

    public BaseDatos(Context context){
        super(context, NOMBRE_BASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA + "( nombre TEXT, calle TEXT, url TEXT, categoria TEXT, latitud TXT, longitud TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS restaurantes");
        onCreate(db);
    }
    public void modificarRestaurante(Restaurante restaurante) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nombre", restaurante.getNombre());
        values.put("calle",restaurante.getCalle());
        values.put("url", restaurante.geturl());
        values.put("categoria",restaurante.getCategoria());
        values.put("longitud",restaurante.getLongitud()+"");
        values.put("latitud",restaurante.getLatitud()+"");

        String[] argumentos = new String[]{String.valueOf(restaurante.getNombre())};
        db.update(TABLA, values, "nombre = ?", argumentos);
        db.close();

    }

    public void eliminarRestaurante(Restaurante restaurante) {

        SQLiteDatabase db = getWritableDatabase();

        String[] argumentos = new String[]{restaurante.getNombre()};
        db.delete(TABLA, "nombre = ?", argumentos);
        db.close();
    }

    public void restauranteFavorito(Restaurante restaurante) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nombre", restaurante.getNombre());
        values.put("calle",restaurante.getCalle());
        values.put("url", restaurante.geturl());
        values.put("categoria",restaurante.getCategoria());
        values.put("longitud",restaurante.getLongitud()+"");
        values.put("latitud",restaurante.getLatitud()+"");
        db.insertOrThrow(TABLA, null, values);
        db.close();
    }
    public ArrayList<Restaurante> getRestaurantesFavoritos() {

        final String[] SELECT = {"nombre", "calle", "url", "categoria",
                "longitud", "latitud"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLA, SELECT, null, null, null, null, null);

        ArrayList<Restaurante> listaRestaurantes = new ArrayList<>();
        Restaurante restaurante = null;
        while (cursor.moveToNext()) {
            restaurante = new Restaurante();
            restaurante.setNombre(cursor.getString(0));
            restaurante.setCalle(cursor.getString(1));
            restaurante.seturl(cursor.getString(2));
            restaurante.setCategoria(cursor.getString(3));
            restaurante.setLongitud(Float.parseFloat(cursor.getString(4)));
            restaurante.setLatitud(Float.parseFloat(cursor.getString(5)));
            listaRestaurantes.add(restaurante);
        }
        db.close();

        return listaRestaurantes;

    }
}
