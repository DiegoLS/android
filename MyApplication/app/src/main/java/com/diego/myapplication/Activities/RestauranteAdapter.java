package com.diego.myapplication.Activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.diego.myapplication.R;

import java.util.ArrayList;

/**
 * Created by borja on 18/12/2016.
 */

public class RestauranteAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Restaurante> listaRestaurante;
    private LayoutInflater inflater;

    public RestauranteAdapter(Activity context, ArrayList<Restaurante> listaRestaurante) {
        this.context = context;
        this.listaRestaurante = listaRestaurante;
        inflater = LayoutInflater.from(context);
    }

    static class ViewHolder {
        TextView nombre;
        TextView calle;
        ImageView foto;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.restaurante, null);

            holder = new ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.calle = (TextView) convertView.findViewById(R.id.tvCalle);
            holder.foto = (ImageView) convertView.findViewById(R.id.ivImagen);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Restaurante restaurante = listaRestaurante.get(position);
        holder.calle.setText(restaurante.getCalle());
        holder.nombre.setText(restaurante.getNombre());
        return convertView;
    }

    @Override
    public int getCount() {
        return listaRestaurante.size();
    }

    @Override
    public Object getItem(int posicion) {
        return listaRestaurante.get(posicion);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

}
