package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.diego.myapplication.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btLogin = (Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(this);
        Button btNuevoUsuario = (Button) findViewById(R.id.btNuevoUsuario);
        btNuevoUsuario.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btLogin:
                Intent intentLogin = new Intent(this, Login.class);
                startActivity(intentLogin);
                break;

            case R.id.btNuevoUsuario:
                Intent intentNuevoUsuario = new Intent(this, NuevoUsuario.class);
                startActivity(intentNuevoUsuario);
                break;
        }
    }
}
