package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.diego.myapplication.Activities.Util.Constantes;
import com.diego.myapplication.R;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class Login extends AppCompatActivity implements View.OnClickListener {

    boolean login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button btEntrarLogin = (Button) findViewById(R.id.btEntrarLogin);
        btEntrarLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        EditText etUsuarioLogin = (EditText) findViewById(R.id.etUsuarioLogin);
        EditText etClaveLogin = (EditText) findViewById(R.id.etClaveLogin);
        switch (view.getId()){
            case R.id.btEntrarLogin:
                String usuarioLogin = etUsuarioLogin.getText().toString();
                String claveLogin = etClaveLogin.getText().toString();
                ServicioWeb servicioWeb = new ServicioWeb();
                servicioWeb.execute(usuarioLogin, claveLogin);
                break;

        }
    }

    private class ServicioWeb extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... strings) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            login = restTemplate.getForObject(Constantes.SERVIDOR_URL + "/user_login?usuario=" + strings[0] + "&pass=" + strings[1], Boolean.class);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(!login){
                Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_login, Toast.LENGTH_SHORT);
                toast.show();
            }else{
                Intent intent = new Intent(getApplicationContext(), ListaRestaurantes.class);
                startActivity(intent);
            }
        }
    }
}
