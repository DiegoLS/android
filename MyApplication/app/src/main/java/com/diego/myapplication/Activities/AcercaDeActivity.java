package com.diego.myapplication.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.diego.myapplication.R;

import static com.diego.myapplication.Activities.Util.Constantes.VERSION;

public class AcercaDeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);
        TextView version=(TextView)findViewById(R.id.tvVersion);
        version.setText(R.string.version+ VERSION);
    }
}
