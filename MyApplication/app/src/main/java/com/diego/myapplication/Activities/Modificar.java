package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.diego.myapplication.R;

public class Modificar extends AppCompatActivity {

    private EditText etcalle;
    private EditText etcategoria;
    private Button aceptar;
    private Button cancelar;
    private Restaurante restaurante;
    BaseDatos db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_restaurante);
        db=new BaseDatos(this);
        restaurante =new Restaurante();
        etcalle=(EditText)findViewById(R.id.etCalle);
        etcategoria=(EditText)findViewById(R.id.etCategoria);
        aceptar=(Button)findViewById(R.id.btModificar);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                restaurante.setCalle(etcalle.getText().toString());
                restaurante.setCategoria(etcategoria.getText().toString());
                db.modificarRestaurante(restaurante);
               Intent intent= new Intent(Modificar.this, ListaFavoritos.class);
                startActivity(intent);
            }
        });
        cancelar=(Button)findViewById(R.id.btCancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(Modificar.this, ListaFavoritos.class);
                startActivity(intent);
            }
        });
        Intent intent = getIntent();

        String nombre = intent.getStringExtra("nombre");
        String calle = intent.getStringExtra("calle");
        String link = intent.getStringExtra("link");
        String categoria = intent.getStringExtra("categoria");
        double latitud = intent.getDoubleExtra("latitud", 0);
        double longitud = intent.getDoubleExtra("longitud", 0);
        restaurante.setNombre(nombre);
        restaurante.setCalle(calle);
        restaurante.seturl(link);
        restaurante.setLatitud(latitud);
        restaurante.setLongitud(longitud);
        restaurante.setCategoria(categoria);
        etcalle.setText(restaurante.getCalle());
        etcategoria.setText(restaurante.getCategoria());

    }
}
