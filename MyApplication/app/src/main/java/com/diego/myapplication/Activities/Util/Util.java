package com.diego.myapplication.Activities.Util;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

/**
 * Created by Diego Lorente on 13/01/2017.
 */
public class Util {

    public static LatLng DeUMTSaLatLng(double este, double oeste){
        UTMRef utm = new UTMRef(este, oeste, 'N', 30);

        return utm.toLatLng();
    }

    public static com.mapbox.mapboxsdk.geometry.LatLng parseCoordenadas(String lat, String longi) {

        // Convierte las coordenadas de UTM a Latitud y Longitud de la librería jcoord
        uk.me.jstott.jcoord.LatLng ubicacion = Util.DeUMTSaLatLng(Double.parseDouble(lat),Double.parseDouble(longi));

        // Devuelve finalmente las coordenadas como un objeto LatLng de Google Maps
        return new com.mapbox.mapboxsdk.geometry.LatLng(ubicacion.getLat(), ubicacion.getLng());
    }
}
