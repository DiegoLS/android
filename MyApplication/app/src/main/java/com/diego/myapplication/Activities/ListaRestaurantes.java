package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.diego.myapplication.R;

import java.util.ArrayList;

public class ListaRestaurantes extends AppCompatActivity {
    private ListView lista;
    public static ArrayList<Restaurante> listaRestaurantes;
    public static RestauranteAdapter adaptador;


    private BaseDatos db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_restaurantes);


        lista = (ListView) findViewById(R.id.lvRestaurantes);
        listaRestaurantes =new ArrayList<>();
        db=new BaseDatos(this);


        actualizarRestaurantes();
        registerForContextMenu(lista);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lista_restaurantes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.action_lista_favoritos:
                intent = new Intent(this, ListaFavoritos.class);
                startActivity(intent);
                break;
            case R.id.action_mapa_todos:
                intent = new Intent(this, MapaTodos.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_lista_restaurantes_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        final int itemSeleccionado = info.position;
        Restaurante restaurante = null;
        Intent intent = null;
        switch (item.getItemId()){
            case R.id.action_favoritos:
                restaurante = listaRestaurantes.get(itemSeleccionado);
                BaseDatos baseDatos = new BaseDatos(this);
                baseDatos.restauranteFavorito(restaurante);
                break;
            case R.id.action_mapa:
                restaurante= (Restaurante) adaptador.getItem(itemSeleccionado);
                intent = new Intent(this, MapActivity.class);
                intent.putExtra("nombre", restaurante.getNombre());
                intent.putExtra("latitud", restaurante.getLatitud());
                intent.putExtra("longitud", restaurante.getLongitud());
                startActivity(intent);
                break;
            case R.id.action_detalles:
                restaurante = (Restaurante) adaptador.getItem(itemSeleccionado);
                intent = new Intent(this, DetallesRestaurante.class);
                intent.putExtra("nombre", restaurante.getNombre());
                intent.putExtra("calle", restaurante.getCalle());
                intent.putExtra("categoria", restaurante.getCategoria());
                startActivity(intent);
                break;
            case R.id.action_modificar:
                intent = new Intent(this, Modificar.class);
                restaurante = (Restaurante) adaptador.getItem(itemSeleccionado);
                intent.putExtra("nombre", restaurante.getNombre());
                intent.putExtra("calle", restaurante.getCalle());
                intent.putExtra("link", restaurante.geturl());
                intent.putExtra("categoria", restaurante.getCategoria());
                intent.putExtra("latitud", restaurante.getLatitud());
                intent.putExtra("longitud",restaurante.getLongitud());
                startActivity(intent);
                break;
        }
        return false;
    }



    @Override
    protected void onResume() {
        super.onResume();

        actualizarRestaurantes();

    }


    public void actualizarRestaurantes(){
        listaRestaurantes =new ArrayList<>();
        adaptador=new RestauranteAdapter(this, listaRestaurantes);
        TareaDescarga tarea = new TareaDescarga(this);
        tarea.execute();

        lista.setAdapter(adaptador);
    }

}