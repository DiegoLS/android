package com.diego.myapplication.Activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.diego.myapplication.Activities.Util.Constantes;
import com.diego.myapplication.R;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class NuevoUsuario extends AppCompatActivity implements View.OnClickListener{

    boolean registro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_usuario);
        Button btAgregarUsuario = (Button) findViewById(R.id.btAgregarUsuario);
        btAgregarUsuario.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        EditText etNuevoUsuario = (EditText) findViewById(R.id.etNuevoUsuario);
        EditText etNuevaClave = (EditText) findViewById(R.id.etNuevaClave);
        switch (view.getId()){
            case R.id.btAgregarUsuario:
                String usuario = etNuevoUsuario.getText().toString();
                String clave = etNuevaClave.getText().toString();
                DescargaDatos descargaDatos = new DescargaDatos();
                descargaDatos.execute(usuario, clave);
        }

    }

    private class DescargaDatos extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... strings) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            registro = restTemplate.getForObject(Constantes.SERVIDOR_URL + "/add_user?usuario=" + strings[0] + "&pass=" + strings[1], Boolean.class);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(!registro){
                Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_nuevoUsuario, Toast.LENGTH_SHORT);
                toast.show();
            }else{
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        }
    }


}
